#!/usr/bin/python
from mandelbrot import MBClient
import os

mb_server = os.environ.get('MB_SERVER', '127.0.0.1')
mb_port   = int( os.environ.get('MB_PORT', 4096) )

print "Server: {0}:{1}".format( mb_server, mb_port )

C=MBClient( host=mb_server, port=mb_port )

