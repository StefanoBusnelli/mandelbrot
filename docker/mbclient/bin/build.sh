#!/bin/bash
printf "\033[1;32m"
echo "*************************"
echo "* Mandelbrot client     *"
echo "*************************"
printf "\033[0m"

docker image rmi --force busnellistefano/mandelbrotclient:latest
docker image rmi --force busnellistefano/mandelbrotclient:$(uname -m)
docker image prune
docker container prune
docker build -t busnellistefano/mandelbrotclient:$(uname -m) .
docker push busnellistefano/mandelbrotclient:$(uname -m)
#docker tag busnellistefano/mandelbrotclient:$(uname -m) busnellistefano/mandelbrotclient:latest
