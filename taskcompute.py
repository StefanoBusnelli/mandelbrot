import math

def taskcompute( task={} ):
  def calcZ( c, maxi ):
    # Dato un numero complesso c calcola il numero di iterazioni affinche'
    # partendo da z = c, il modulo di z = z*z + c diventa > 2
    i = 1
    z = c
    while( i < maxi and abs( z )<2.0 ):
      i = i + 1
      z = z * z + c

    return [i, z]

  if len( task ) > 0:
    c = complex( task['data'][0], task['data'][1] )
    m = task['data'][2] 
  
    #print("Calcolo {0} un numero massimo di {1} iterazioni.".format( c, m  ) )
    r = calcZ( c, m )
  
    return r

  else:
    print( "Vuoto" )
    
    return None

